print("Begin")
print("===========================")

# Input variables
# ------------------------------------------------------------------------------------------------
vw='3d';	#	Viewpoint for plotting ('3d', )
Npf=0; 		# 	Node label plot flag (1 for labels, 0 for none)
Epf=0; 		# 	Element label plot flag (1 for labels, 0 for none)
LPpf=1; 	# 	Load pattern plot flag (1 for labels, 0 for none)
eig=0; 		#	number of mode shapes, 0 to do nothing

# Define the file names
# ------------------------------------------------------------------------------------------------
fname_model="model.txt";

# Compute the element and load-pattern co-ordinates
# ------------------------------------------------------------------------------------------------
element.def_ele_coord(node);
if LPpf==1:
	load_pattern.def_load_pattern_coord(node);

# Plot the model using Matplotlib
# ------------------------------------------------------------------------------------------------
# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
# fig=plt.figure();
# ax=fig.add_subplot(111, projection=vw);
# # Plot the nodes
# for i in range(len(node.num)):
# 	# Get node color depending on the presence of mass or not
# 	if node.Xmass[i]>0 or node.Ymass[i]>0 or node.Zmass[i]>0:
# 		node_sz=30;
# 		node_clr='k';
# 		node_shp='o';
# 	else:
# 		node_clr='k';
# 		node_sz=5;
# 		node_shp='s';
# 	ax.scatter(node.Xcoord[i]+node.Xamp*node.Xdisp[i],node.Ycoord[i]+node.Yamp*node.Ydisp[i],node.Zcoord[i]+node.Zamp*node.Zdisp[i],color=node_clr,s=node_sz,marker=node_shp);
# # Plot the elements
# for i in range(len(element.num)):
# 	# Set up the color of the element types
# 	if element.typ[i]=='ForceBeamColumn3d':
# 		ele_clr='b';
# 	elif element.typ[i]=='Truss':
# 		ele_clr='g';
# 	elif element.typ[i]=='ElasticBeam3d':
# 		ele_clr='k';
# 	elif element.typ[i]=='ZeroLength':
# 		ele_clr='m';
# 	ax.plot([element.iXcoord[i], element.jXcoord[i]],[element.iYcoord[i], element.jYcoord[i]],[element.iZcoord[i], element.jZcoord[i]],linestyle='-',color=ele_clr,linewidth=1);
# if LPpf==1:
# 	# From here: https://stackoverflow.com/questions/11140163/python-matplotlib-plotting-a-3d-cube-a-sphere-and-a-vector/11156353#11156353
# 	from matplotlib.patches import FancyArrowPatch
# 	from mpl_toolkits.mplot3d import proj3d
# 	class Arrow3D(FancyArrowPatch):
#
# 	    def __init__(self, xs, ys, zs, *args, **kwargs):
# 	        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
# 	        self._verts3d = xs, ys, zs
#
# 	    def draw(self, renderer):
# 	        xs3d, ys3d, zs3d = self._verts3d
# 	        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
# 	        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
# 	        FancyArrowPatch.draw(self, renderer)
#
# 	# Plot the load patterns
# 	for i in range(len(load_pattern.num)):
# 		for j in range(len(load_pattern.node[i])):
# 			if load_pattern.fX[i][j]!=0:
# 				a = Arrow3D([load_pattern.Xcoord[i][j]-np.sign(load_pattern.fX[i][j])*1.0, load_pattern.Xcoord[i][j]], [load_pattern.Ycoord[i][j], load_pattern.Ycoord[i][j]], [load_pattern.Zcoord[i][j], load_pattern.Zcoord[i][j]], mutation_scale=20,lw=1, arrowstyle="-|>", color="r");
# 				ax.add_artist(a);
# 			elif load_pattern.fY[i][j]!=0:
# 				a = Arrow3D([load_pattern.Xcoord[i][j], load_pattern.Xcoord[i][j]], [load_pattern.Ycoord[i][j]-np.sign(load_pattern.fY[i][j])*1.0, load_pattern.Ycoord[i][j]], [load_pattern.Zcoord[i][j], load_pattern.Zcoord[i][j]], mutation_scale=20,lw=1, arrowstyle="-|>", color="r");
# 				ax.add_artist(a);
# 			elif load_pattern.fZ[i][j]!=0:
# 				a = Arrow3D([load_pattern.Xcoord[i][j], load_pattern.Xcoord[i][j]], [load_pattern.Ycoord[i][j], load_pattern.Ycoord[i][j]], [load_pattern.Zcoord[i][j]-np.sign(load_pattern.fZ[i][j])*1.0, load_pattern.Zcoord[i][j]], mutation_scale=20,lw=1, arrowstyle="-|>", color="r");
# 				ax.add_artist(a);
# ax.set_xlabel('X');
# ax.set_ylabel('Y');
# ax.set_zlabel('Z');
# plt.show();

print("This is the end of the file")
print("===========================")
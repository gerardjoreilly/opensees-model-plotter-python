# Plotting tool for OpenSees models using Python
# Created: November 2017
# Gerard O'Reilly
# Copyright by Gerard J. O'Reilly, 2017

print("Begin")
print("===========================")

# Import some libraries
import os
import numpy as np

# Input variables
# ------------------------------------------------------------------------------------------------
vw='3d';	#	Viewpoint for plotting ('3d', )
Npf=0; 		# 	Node label plot flag (1 for labels, 0 for none)
Epf=0; 		# 	Element label plot flag (1 for labels, 0 for none)
LPpf=1; 	# 	Load pattern plot flag (1 for labels, 0 for none)
eig=0; 		#	number of mode shapes, 0 to do nothing

# Define the file names
# ------------------------------------------------------------------------------------------------
# fname_model="model.txt";
# fname_model="/Users/Gerard/GDrive/ProgettoScuoleGerard/ModalAnalysis/Carrara_model_v5.txt"
# fname_model="/Users/Gerard/GDrive/ProgettoScuoleGerard/ModalAnalysis/Ancona_model_v5.txt"
fname_model="/Users/Gerard/GDrive/PhD/Modelling/RC_pre1970/Galli/SPO/info/Galli_model_6st_Bare.txt";
# fname_model="/Users/Gerard/GDrive/OpenSees/GLD_frames/outs_Galli_SPO/info/models/Galli_model_2st_Bare.txt";
# fname_eigen="eigenVectors_modal.txt";
# fname_periods="Periods_modal.txt";




# Plot the model using Plotly
# ------------------------------------------------------------------------------------------------
import plotly.plotly as py
import plotly.tools as plyt
import plotly.graph_objs as go
plyt.set_credentials_file(username='gerard.oreilly', api_key='SMgMODaCoZjC7LgYJsHY')


# Plot the nodes
data1=[]
for i in range(len(node.num)):
	# Get node color depending on the presence of mass or not
	if node.Xmass[i]>0 or node.Ymass[i]>0 or node.Zmass[i]>0:
		node_sz=10;
		node_clr='rgb(0,0,0)';
		node_shp='o';
	else:
		node_clr='rgb(255,0,0)';
		node_sz=5;
		node_shp='s';
	trace=go.Scatter3d(x=node.Xcoord[i]+node.Xamp*node.Xdisp[i],y=node.Ycoord[i]+node.Yamp*node.Ydisp[i],z=node.Zcoord[i]+node.Zamp*node.Zdisp[i],mode='markers',marker=dict(color=node_clr,size=node_sz));
	data1.append(trace)

# Plot the elements
for i in range(len(element.num)):
	# Set up the color of the element types
	if element.typ[i]=='ForceBeamColumn3d':
		ele_clr='rgb(0,0,255)';
	elif element.typ[i]=='Truss':
		ele_clr='rgb(0,128,0)';
	elif element.typ[i]=='ElasticBeam3d':
		ele_clr='rgb(0,0,0)';
	elif element.typ[i]=='ZeroLength':
		ele_clr='rgb(255,0,255)';
	trace=go.Scatter3d(x=[element.iXcoord[i], element.jXcoord[i]],y=[element.iYcoord[i], element.jYcoord[i]],z=[element.iZcoord[i], element.jZcoord[i]],mode='lines',line=dict(color=ele_clr));
	data1.append(trace)

layout = go.Layout(yaxis=dict(scaleanchor="x", scaleratio=10))

fig = go.Figure(data=data1, layout=layout)
py.iplot(fig, filename='OpenSees-Model-Plotter-Python-Demo2')






print("This is the end of the file")
print("===========================")

# Plotting tool for OpenSees models using Python
# Created: November 2017
# Gerard O'Reilly
# Copyright by Gerard J. O'Reilly, 2017


# Define the classes
# ------------------------------------------------------------------------------------------------
class node:
    Xamp = 5;  # Amplification on Xcoord disps
    Yamp = 5;  # Amplification on Ycoord disps
    Zamp = 5;  # Amplification on Zcoord disps

    num = [];
    Xcoord = [];
    Ycoord = [];
    Zcoord = [];
    Xdisp = [];
    Ydisp = [];
    Zdisp = [];
    Xmass = [];
    Ymass = [];
    Zmass = [];

    # Create class method to append new nodes
    @classmethod
    def add_node(self, num, Xcoord, Ycoord, Zcoord, Xdisp, Ydisp, Zdisp, Xmass, Ymass, Zmass):
        self.num.append(num);
        self.Xcoord.append(Xcoord);
        self.Ycoord.append(Ycoord);
        self.Zcoord.append(Zcoord);
        self.Xdisp.append(Xdisp);
        self.Ydisp.append(Ydisp);
        self.Zdisp.append(Zdisp);
        self.Xmass.append(Xmass);
        self.Ymass.append(Ymass);
        self.Zmass.append(Zmass);

class element(node):
    # Initialise the lists within the class
    typ = [];
    num = [];
    iNode = [];
    jNode = [];
    iXcoord = [];
    iYcoord = [];
    iZcoord = [];
    jXcoord = [];
    jYcoord = [];
    jZcoord = [];

    # Create class method to append new elements
    @classmethod
    def add_element(self, typ, num, iNode, jNode):
        self.typ.append(typ);
        self.num.append(num);
        self.iNode.append(iNode);
        self.jNode.append(jNode);

    # Create class method to identify element co-ordinates
    @classmethod
    def def_ele_coord(self, node):
        for i in range(len(self.num)):
            idi = node.num.index(self.iNode[i]);
            idj = node.num.index(self.jNode[i]);

            self.iXcoord.append(node.Xcoord[idi] + node.Xamp * node.Xdisp[idi]);
            self.iYcoord.append(node.Ycoord[idi] + node.Yamp * node.Ydisp[idi]);
            self.iZcoord.append(node.Zcoord[idi] + node.Zamp * node.Zdisp[idi]);
            self.jXcoord.append(node.Xcoord[idj] + node.Xamp * node.Xdisp[idj]);
            self.jYcoord.append(node.Ycoord[idj] + node.Yamp * node.Ydisp[idj]);
            self.jZcoord.append(node.Zcoord[idj] + node.Zamp * node.Zdisp[idj]);


if LPpf == 1:
    class load_pattern(node):
        num = [];
        typ = [];
        node = [];
        fX = [];
        fY = [];
        fZ = [];
        mX = [];
        mY = [];
        mZ = [];
        Xcoord = [];
        Ycoord = [];
        Zcoord = [];

        # Create class method to append new load patterns
        @classmethod
        def add_load_pattern(self, num, typ, node, fX, fY, fZ, mX, mY, mZ):
            self.num.append(num);
            self.typ.append(typ);
            self.node.append(node);
            self.fX.append(fX);
            self.fY.append(fY);
            self.fZ.append(fZ);
            self.mX.append(mX);
            self.mY.append(mY);
            self.mZ.append(mZ);

        # Create class method to identify load patterns coordinates
        @classmethod
        def def_load_pattern_coord(self, node):
            for i in range(len(self.num)):
                temp_coord_X = [];
                temp_coord_Y = [];
                temp_coord_Z = [];
                for j in range(len(self.node[i])):
                    idx = node.num.index(self.node[i][j]);
                    temp_coord_X.append(node.Xcoord[idx] + node.Xamp * node.Xdisp[idx]);
                    temp_coord_Y.append(node.Ycoord[idx] + node.Yamp * node.Ydisp[idx]);
                    temp_coord_Z.append(node.Zcoord[idx] + node.Zamp * node.Zdisp[idx]);

                self.Xcoord.append(temp_coord_X);
                self.Ycoord.append(temp_coord_Y);
                self.Zcoord.append(temp_coord_Z);

# Start reading the model file
# ------------------------------------------------------------------------------------------------
with open(fname_model, "r") as model_file:
    for x in model_file:
        # Get the nodes
        if x.find(" Node: ") == 0:
            node_num = x.split()[1];  # Take second then turn remainder to integer
            x1 = next(model_file);  # Get the next line
            Xcoord = float(x1.split()[2]);  # Get the x co-ordinate
            Ycoord = float(x1.split()[3]);  # Get the y co-ordinate
            Zcoord = float(x1.split()[4]);  # Get the z co-ordinate
            x1 = next(model_file);  # Get the next line
            Xdisp = float(x1.split()[1]);  # Get the x co-ordinate
            Ydisp = float(x1.split()[2]);  # Get the x co-ordinate
            Zdisp = float(x1.split()[3]);  # Get the x co-ordinate
            for i in range(8):
                x1 = next(model_file);
                if i == 4:
                    Xmass = float(x1.split()[0]);  # Get the x mass
                elif i == 5:
                    Ymass = float(x1.split()[1]);  # Get the y mass
                elif i == 6:
                    Zmass = float(x1.split()[2]);  # Get the z mass
            node.add_node(node_num, Xcoord, Ycoord, Zcoord, Xdisp, Ydisp, Zdisp, Xmass, Ymass, Zmass);

        # Get the elements
        # Look for Elements
        if x.find("Element:") == 0:
            ele_num = x.split()[1];
            ele_type = x.split()[3];
            if ele_type == "ForceBeamColumn3d":
                ele_iNode = x.split()[6];
                ele_jNode = x.split()[7];
            elif ele_type == "ZeroLength":
                ele_iNode = x.split()[5];
                ele_jNode = x.split()[7];
            elif ele_type == "Truss":
                ele_iNode = x.split()[5];
                ele_jNode = x.split()[7];
            element.add_element(ele_type, ele_num, ele_iNode, ele_jNode);

        # Look for ElasticBeam3d
        if x.find("ElasticBeam3d") == 0:
            ele_type = "ElasticBeam3d";
            ele_num = x.split()[1];  # This splits takes the second and converts it to an integer
            x1 = next(model_file);  # This takes the next line of the modelfile
            ele_iNode = x1.split()[2];  # Take first
            ele_jNode = x1.split()[3];  # Take second
            element.add_element(ele_type, ele_num, ele_iNode, ele_jNode);

        # Look for load patterns
        if LPpf == 1:
            if x.find("Load Pattern: ") == 0:
                lp_num = x.split()[2];
                # Shift down two lines to get load pattern type
                x1 = next(model_file);
                x1 = next(model_file);
                lp_typ = x1.split()[0];
                x1 = next(model_file);
                x1 = next(model_file);
                lp_node = [];
                lp_fX = [];
                lp_fY = [];
                lp_fZ = [];
                lp_mX = [];
                lp_mY = [];
                lp_mZ = [];
                while x1.find("Nodal Load: ") == 0:
                    lp_node.append(x1.split()[2]);
                    lp_fX.append(float(x1.split()[5]));
                    lp_fY.append(float(x1.split()[6]));
                    lp_fZ.append(float(x1.split()[7]));
                    lp_mX.append(float(x1.split()[8]));
                    lp_mY.append(float(x1.split()[9]));
                    lp_mZ.append(float(x1.split()[10]));
                    x1 = next(model_file);
                load_pattern.add_load_pattern(lp_num, lp_typ, lp_node, lp_fX, lp_fY, lp_fZ, lp_mX, lp_mY, lp_mZ);
